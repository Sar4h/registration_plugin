<?php

/**
 * @package register_users
 */
/*
Plugin Name: register_users
Description: This plugin enables users to register with a subscriber role.
Version: 1.0.0
Author: Nakyembe Sarah
License: GPLv2 or later
Text Domain: register_users
*/

add_action('user_register', 'save_additional_fields');
add_action('show_user_profile', 'additional_fields');
add_action('edit_user_profile', 'additional_fields');
add_action('personal_options_update', 'save_additional_fields');
add_action('edit_user_profile_update', 'save_additional_fields');
function registration_form($firstname, $lastname, $gender, $date_of_birth, $place_of_birth, $bio, $username, $password)
{
    echo '
<style>
div{
margin-bottom:2px;
}
input{
margin-bottom:4px;
}
</style>';
    echo '
<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
  <div>
    <p>Please fill in this form to create an account.</p>
    <hr>
</div>
<div>
    <label for="firstname"><b> First name *</b></label>
    <input type="text" name="firstname" value="' . (isset($_POST['firstname']) ? $firstname : null) . '"  required>
</div>
<div>
    <label for="lastname"><b> Last name *</b></label>
    <input type="text" name="lastname" value="' . (isset($_POST['lastname']) ? $lastname : null) . '" required>
</div>
<div>
    <label for="username"><b>Username *</b></label>
    <input type="text"  name="username" value="' . (isset($_POST['username']) ? $username : null) . '" required>
</div>
<div>
    <label for="password"><b>Password *</b></label>
    <input type="password" name="password"  value="' . (isset($_POST['password']) ? $password : null) . '"  required>
</div>
<div>
    <label for="gender"><b> Gender *</b></label>
    <input type="text" name="gender" value="' . (isset($_POST['gender']) ? $gender : null) . '" required>
</div>
<div>
    <label for="date_of_birth"><b> Date of Birth *</b></label>
    <input type="date" name="date_of_birth" value="' . (isset($_POST['date_of_birth']) ? $date_of_birth : null) . '" required>
</div>
<div>
    <label for="place_of_birth"><b> Place of Birth *</b></label>
    <input type="text" name="place_of_birth" value="' . (isset($_POST['place_of_birth']) ? $place_of_birth : null) . '"  required>
</div>
<div>
    <label for="bio"><b> Short Bio *</b></label>
<textarea name="bio">' . (isset($_POST['bio']) ? $bio : null) . '</textarea>
</div>

      <input type="submit" name="submit" value="Register"/>
   </form>
';
}
function registration_validation($firstname, $lastname, $gender, $date_of_birth, $place_of_birth, $bio, $username, $password)
{
    global $reg_errors;
    $reg_errors = new WP_Error;
    if (4 > strlen($username)) {
        $reg_errors->add('username_length', 'username too short. Atleats  4 characters required');
    }
    if (username_exists($username)) {
        $reg_errors->add('username', 'Sorry, that username already exists!');
    }
    if (!validate_username($username)) {
        $reg_errors->add('username_invalid', 'Sorry, the username you entered is not valid');
    }
    if (5 > strlen($password)) {
        $reg_errors->add('password', 'Password length must be greater than 5');
    }
    if (empty($date_of_birth)) {
        $reg_errors->add('year_of_birth_Error', 'Please enter your year of birth');
    }
    if (empty($place_of_birth)) {
        $reg_errors->add('Place_of_birth_Error', 'Please enter your place of birth');
    }
    if (empty($gender)) {
        $reg_errors->add('gender', 'Please enter your gender');
    }
    if (empty($firstname)) {
        $reg_errors->add('firstname', 'Please enter your firstname');
    }
    if (empty($lastname)) {
        $reg_errors->add('lastname', 'Please enter your lasstname');
    }
    if (is_wp_error($reg_errors)) {
        foreach ($reg_errors->get_error_messages() as $error) {
            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo  $error . '<br/>';
            echo '<div/>';
        }
    }
}
function complete_registration()
{
    global $reg_errors, $firstname, $lastname, $bio, $username, $password;
    if (1 > count($reg_errors->get_error_messages())) {
        $userdata = array(
            'user_login' => $username,
            'user_pass' => $password,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'description' => $bio,
        );
        wp_insert_user($userdata);
    }
    echo '<div>Registration complete. Go to <a href="' . get_site_url() . '/wp-login.php">login</a>.</div>';
}

function custom_registration()
{
    global $firstname, $lastname, $gender, $date_of_birth, $place_of_birth, $bio, $username, $password;
    if (isset($_POST['submit'])) {
        registration_validation(
            $_POST['firstname'],
            $_POST['lastname'],
            $_POST['gender'],
            $_POST['date_of_birth'],
            $_POST['place_of_birth'],
            $_POST['bio'],
            $_POST['username'],
            $_POST['password']
        );

        $username = sanitize_user($_POST['username']);
        $password = esc_attr($_POST['password']);
        $firstname = sanitize_text_field($_POST['firstname']);
        $lastname = sanitize_text_field($_POST['lastname']);
        $bio = esc_textarea($_POST['bio']);

        complete_registration($firstname, $lastname, $bio, $username, $password);
    }
    registration_form($firstname, $lastname, $gender, $date_of_birth, $place_of_birth, $bio, $username, $password);
}
add_shortcode('registration_form', 'custom_registration_shortcode');
function custom_registration_shortcode()
{
    ob_start();
    custom_registration();
    return ob_get_clean();
}

function save_additional_fields($user_id)
{
   /*  if (!current_user_can('edit_user', $user_id)) {
        return false;
    } */
    update_user_meta($user_id, 'gender', $_POST['gender']);
    update_user_meta($user_id, 'date_of_birth', $_POST['date_of_birth']);
    update_user_meta($user_id, 'place_of_birth', $_POST['place_of_birth']);
}

function additional_fields($user)
{
    $gender = esc_attr(get_the_author_meta('gender', $user->ID));
    $date_of_birth = esc_attr(get_the_author_meta('date_of_birth', $user->ID));
    $place_of_birth = esc_attr(get_the_author_meta('place_of_birth', $user->ID));
?>
    <h3>Extra profile information</h3>
    <table class="form-table">
        <tr>
            <th><label for="gender"><b> Gender </b></label></th>
            <td>
                <input type="text" name="gender" id="gender" value="<?php echo  $gender; ?>" class="regular-text" /></td>
        </tr>
        <tr>
            <th><label for="date_of_birth"><b>Date of Birth </b></label></th>
            <td><input type="text" id="date_of_birth" name="date_of_birth" value="<?php echo $date_of_birth; ?>" class="regular-text" /></td>
        </tr>
        <tr>
            <th><label for="place_of_birth"><b>Place of Birth </b></label></th>
            <td><input type="text" name="place_of_birth" id="place_of_birth" value="<?php echo $place_of_birth; ?>" class="regular-text" /></td>
        </tr>
    </table>
<?php
}?>
<?php
/*custom_registration();*/
?>